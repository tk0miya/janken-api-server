package main

import (
	"bufio"
	"flag"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/tk0miya/janken-api-server/pkg/http/handler"
)

type Command int

const (
	CmdQuit Command = iota
	CmdPong
)

func main() {
	interval := flag.Int("interval", 30, "int flag")
	flag.Parse()

	ticker := time.NewTicker(time.Duration(*interval) * time.Second)
	done := make(chan bool)

	prompt := make(chan Command)
	go ReadCommandPrompt(prompt)

	go func() {
		for {
			select {
			case <-done:
				return
			case command := <-prompt:
				switch command {
				case CmdQuit:
					os.Exit(0)
				case CmdPong:
					Pong()
				}
			case <-ticker.C:
				Pong()
			}
		}
	}()

	log.Printf("Listening on 0.0.0.0:8080 ...")
	http.HandleFunc("/hand", handler.Entry)
	http.ListenAndServe(":8080", nil)
}

func ReadCommandPrompt(event chan<- Command) {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		scanner.Scan()
		command := scanner.Text()
		switch command {
		case "/pong":
			event <- CmdPong
		case "/quit":
			event <- CmdQuit
		default:
			log.Printf("Unknown command: %v", command)
		}
	}
}

func Pong() {
	winners := handler.Pong()
	if len(winners) == 0 {
		log.Printf("No winners.")
	} else {
		for _, winner := range winners {
			log.Printf("%s wins by %s", winner.Name, winner.Hand)
		}
	}
	handler.Reset()
}
