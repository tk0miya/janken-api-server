.PHONY: all
all: janken-api-server.macos janken-api-server.linux janken-api-server.exe

.PHONY: clean
clean:
	rm -f janken-api-server.macos janken-api-server.linux janken-api-server.exe

.PHONY: janken-api-server.macos
janken-api-server.macos:
	GOOS=darwin go build -o janken-api-server.macos cmd/janken_api_server/main.go

.PHONY: janken-api-server.linux
janken-api-server.linux:
	GOOS=linux go build -o janken-api-server.linux cmd/janken_api_server/main.go

.PHONY: janken-api-server.exe
janken-api-server.exe:
	GOOS=windows GOARCH=amd64 go build -o janken-api-server.exe cmd/janken_api_server/main.go