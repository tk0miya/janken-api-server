package handler

import (
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/tk0miya/janken-api-server/pkg/janken"
)

func TestPlayer_validate(t *testing.T) {
	type fields struct {
		Name string
		Hand janken.Hand
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "valid",
			fields:  fields{"Taro", janken.Guu},
			wantErr: false,
		},
		{
			name:    "name: empty",
			fields:  fields{"", janken.Choki},
			wantErr: true,
		},
		{
			name:    "hand: out of candidates",
			fields:  fields{"Taro", 0},
			wantErr: true,
		},
		{
			name:    "hand: out of candidates",
			fields:  fields{"Taro", 4},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Player{
				Name: tt.fields.Name,
				Hand: tt.fields.Hand,
			}
			if err := p.validate(); (err != nil) != tt.wantErr {
				t.Errorf("Player.validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestEntry_GET(t *testing.T) {
	tests := []struct {
		name    string
		players []Player
		code    int
		result  string
	}{
		{
			name:    "empty",
			players: []Player{},
			code:    404,
			result:  "",
		},
		{
			name:    "invalid hand",
			players: []Player{{"Taro", janken.Guu}, {"Jiro", janken.Choki}},
			code:    200,
			result:  `["Taro","Jiro"]` + "\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Reset()
			for _, player := range tt.players {
				AddPlayer(player)
			}

			w := httptest.NewRecorder()
			r := httptest.NewRequest("GET", "/hand", nil)
			Entry(w, r)
			if w.Code != tt.code {
				t.Errorf("Entry() code = %d, expected %d", w.Code, tt.code)
			}
			if w.Body.String() != tt.result {
				t.Errorf("%s vs %s", w.Body.String(), tt.result)
				t.Errorf("Entry() Body = %s, expected %s", w.Body.String(), tt.result)
			}
		})
	}
}

func TestEntry_POST(t *testing.T) {
	tests := []struct {
		name    string
		content string
		code    int
	}{
		{
			name:    "valid",
			content: `{"player_name": "Taro", "hand": 1}`,
			code:    204,
		},
		{
			name:    "invalid hand",
			content: `{"player_name": "Taro", "hand": 0}`,
			code:    400,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			content := strings.NewReader(tt.content)
			w := httptest.NewRecorder()
			r := httptest.NewRequest("POST", "/hand", content)
			Entry(w, r)
			if w.Code != tt.code {
				t.Errorf("Entry() code = %d, expected %d", w.Code, tt.code)
			}
		})
	}
}

func TestPong(t *testing.T) {
	AddPlayer(Player{"Taro", janken.Guu})
	AddPlayer(Player{"Jiro", janken.Choki})
	tests := []struct {
		name    string
		players []Player
		winners []Player
	}{
		{
			name:    "valid",
			players: []Player{{"Taro", janken.Guu}, {"Jiro", janken.Choki}},
			winners: []Player{{"Taro", janken.Guu}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Reset()
			for _, player := range tt.players {
				AddPlayer(player)
			}
			winners := Pong()
			if len(winners) != len(tt.winners) {
				t.Errorf("Pong() winners = %v, expected %v", winners, tt.winners)
			}
			for i, winner := range tt.winners {
				if winners[i].Name != winner.Name {
					t.Errorf("Pong() winners[i] = %v, expected %v", winners[i], winner)
				}
			}
		})
	}
}
