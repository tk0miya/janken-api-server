package handler

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"

	"gitlab.com/tk0miya/janken-api-server/pkg/janken"
)

type Player struct {
	Name string      `json:"player_name"`
	Hand janken.Hand `json:"hand"`
}

func (p *Player) validate() error {
	if p.Name == "" {
		return errors.New("name is required")
	}

	if int(p.Hand) < 1 || 3 < int(p.Hand) {
		return errors.New("hand should be between 1 to 3")
	}

	return nil
}

var players []Player

func Entry(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		if len(players) == 0 {
			w.WriteHeader(404)
		} else {
			var names []string
			for _, player := range players {
				names = append(names, player.Name)
			}

			json.NewEncoder(w).Encode(names)
		}
	case "POST":
		var player Player
		json.NewDecoder(r.Body).Decode(&player)
		if err := player.validate(); err != nil {
			w.WriteHeader(400)
			return
		}

		AddPlayer(player)
		log.Printf("player %s joined\n", player.Name)
		w.WriteHeader(204)
	default:
		w.WriteHeader(400)
	}
}

func Reset() {
	players = nil
}

func AddPlayer(player Player) {
	players = append(players, player)
}

func Pong() []Player {
	hands := []janken.Hand{}
	for _, player := range players {
		hands = append(hands, player.Hand)
	}

	winners := []Player{}
	winning_hand, ok := janken.Pong(hands)
	if ok {
		for _, player := range players {
			if player.Hand == winning_hand {
				winners = append(winners, player)
			}
		}
	}
	return winners
}
