package janken

import "testing"

func Test_Pong(t *testing.T) {
	// 人数不足
	hands := []Hand{}
	if hand, ok := Pong(hands); ok == true {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Guu}
	if hand, ok := Pong(hands); ok == true {
		t.Errorf("winners found: %s", hand)
	}

	// あいこ
	hands = []Hand{Guu, Guu}
	if hand, ok := Pong(hands); ok == true {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Guu, Choki, Paa}
	if hand, ok := Pong(hands); ok == true {
		t.Errorf("winners found: %s", hand)
	}

	// 勝ち
	hands = []Hand{Guu, Choki}
	if hand, ok := Pong(hands); hand != Guu || ok == false {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Choki, Guu}
	if hand, ok := Pong(hands); hand != Guu || ok == false {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Choki, Paa}
	if hand, ok := Pong(hands); hand != Choki || ok == false {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Paa, Guu}
	if hand, ok := Pong(hands); hand != Paa || ok == false {
		t.Errorf("winners found: %s", hand)
	}

	hands = []Hand{Paa, Paa, Guu, Guu}
	if hand, ok := Pong(hands); hand != Paa || ok == false {
		t.Errorf("winners found: %s", hand)
	}
}
