package janken

type Hand int

const (
	Guu Hand = iota + 1
	Choki
	Paa
)

func (hand Hand) String() string {
	switch hand {
	case Guu:
		return "Guu"
	case Choki:
		return "Choki"
	case Paa:
		return "Paa"
	default:
		return "Other"
	}
}

func Pong(hands []Hand) (Hand, bool) {
	uniqHands := unique(hands)
	switch len(uniqHands) {
	case 2:
		return judge(uniqHands[0], uniqHands[1]), true
	default:
		return 0, false
	}
}

func unique(hands []Hand) []Hand {
	uniqHandMap := make(map[Hand]struct{})
	for _, hand := range hands {
		uniqHandMap[hand] = struct{}{}
	}

	uniqHands := []Hand{}
	for hand := range uniqHandMap {
		uniqHands = append(uniqHands, hand)
	}

	return uniqHands
}

func judge(hand1 Hand, hand2 Hand) Hand {
	if hand1 == hand2 {
		return 0
	} else if hand1-hand2 == -1 || hand1 == 3 && hand2 == 1 {
		return hand1
	} else {
		return hand2
	}
}
